package org.example;

public class PriceSystem {
    public static final int PRICE_POR_KILOMETER = 35; // В гривнях
    final int maxDistance = 90;

    public void priceCounter(Registration costumer) throws ZeroAgeException, DistanceException {
        double price = costumer.getDistance() * PRICE_POR_KILOMETER;
        if (costumer.getDistance() > maxDistance){
            throw new DistanceException("Максимальна дистанція для цього такці становить 90 кілометрів");
        }
        if (costumer.getAge() <= 0) {
            throw new ZeroAgeException("Вам не може бути 0 років, будь ласка введіть дійсний вік");
        } else {
            System.out.println("Добрий день, " + costumer.getName() + " ціна за проїзд становить : " + price + " UAH");
        }
    }
}

