package org.example;

public class Main {
    public static void main(String[] args) throws ZeroAgeException, DistanceException {
        PriceSystem priceSystem = new PriceSystem();
        User123 user123 = new User123("John", 12,10);
        priceSystem.priceCounter(new Registration(user123.getName(), user123.getAge(), user123.getDistance()));
    }

}
